const servicesItems = document.querySelectorAll('.services-item');
const servicesTexts = document.querySelectorAll('.services-text');
const servicesImages = document.querySelectorAll('.services-img');

servicesTexts.forEach((text, i) => text.style.display = i === 0 ? 'block' : 'none');
servicesImages.forEach((img, i) => img.style.display = i === 0 ? 'block' : 'none');

servicesItems.forEach(item => {
    item.addEventListener('click', event => {
        const number = event.target.getAttribute('data-number');

        document.querySelector('.services-item.active').classList.remove('active');
        event.target.classList.add('active');

        servicesTexts.forEach(text => text.style.display = 'none');
        servicesImages.forEach(img => img.style.display = 'none');

        document.querySelector(`.services-text[data-number="${number}"]`).style.display = 'block';
        document.querySelector(`.services-img[data-number="${number}"]`).style.display = 'block';
    });
});



    const loadMore = [
        {
            url: "/img/amazing/amazing-2.jpg",
            dataAttributes: {
                "data-design": "2"
            }
        },
        {
            url: "/img/amazing/amazing-3.jpg",
            dataAttributes: {
                "data-design": "3"
            }
        },
        {
            url: "/img/amazing/amazing-4.jpg",
            dataAttributes: {
                "data-design": "4"
            }
        },
        {
            url: "/img/amazing/amazing-5.jpg",
            dataAttributes: {
                "data-design": "5"
            }
        },
        {
            url: "/img/amazing/amazing-6.jpg",
            dataAttributes: {
                "data-design": "2"
            }
        },
        {
            url: "/img/amazing/amazing-7.jpg",
            dataAttributes: {
                "data-design": "3"
            }
        },
        {
            url: "/img/amazing/amazing-8.jpg",
            dataAttributes: {
                "data-design": "4"
            }
        },
        {
            url: "/img/amazing/amazing-9.jpg",
            dataAttributes: {
                "data-design": "5"
            }
        },
        {
            url: "/img/amazing/amazing-10.jpg",
            dataAttributes: {
                "data-design": "4"
            }
        },
        {
            url: "/img/amazing/amazing-11.jpg",
            dataAttributes: {
                "data-design": "2"
            }
        },
        {
            url: "/img/amazing/amazing-12.jpg",
            dataAttributes: {
                "data-design": "3"
            }
        },
        {
            url: "/img/amazing/amazing-13.jpg",
            dataAttributes: {
                "data-design": "5"
            }
        },
        {
            url: "/img/graphic-design/graphic-design1.jpg",
            dataAttributes: {
                "data-design": "2"
            }
        },
        {
            url: "/img/graphic-design/graphic-design2.jpg",
            dataAttributes: {
                "data-design": "4"
            }
        },
        {
            url: "/img/graphic-design/graphic-design3.jpg",
            dataAttributes: {
                "data-design": "3"
            }
        },
        {
            url: "/img/graphic-design/graphic-design4.jpg",
            dataAttributes: {
                "data-design": "5"
            }
        },
        {
            url: "/img/graphic-design/graphic-design5.jpg",
            dataAttributes: {
                "data-design": "2"
            }
        },
        {
            url: "/img/graphic-design/graphic-design6.jpg",
            dataAttributes: {
                "data-design": "5"
            }
        },
        {
            url: "/img/graphic-design/graphic-design7.jpg",
            dataAttributes: {
                "data-design": "4"
            }
        },
        {
            url: "/img/graphic-design/graphic-design8.jpg",
            dataAttributes: {
                "data-design": "3"
            }
        },
        {
            url: "/img/graphic-design/graphic-design9.jpg",
            dataAttributes: {
                "data-design": "4"
            }
        },
        {
            url: "/img/graphic-design/graphic-design10.jpg",
            dataAttributes: {
                "data-design": "5"
            }
        },
        {
            url: "/img/graphic-design/graphic-design11.jpg",
            dataAttributes: {
                "data-design": "2"
            }
        },
        {
            url: "/img/graphic-design/graphic-design12.jpg",
            dataAttributes: {
                "data-design": "3"
            }
        },
    ]

let amazingItems = document.querySelectorAll('.amazing-item');
let loadButton = document.querySelector('.load-more');
const container = document.querySelector('.amazing-images');

const resStr = (attribute) => {
    switch(attribute){
        case "2":return "Graphic design";
        case '3':return "Web Design";
        case '4': return "Landing Pages";
        case '5': return  "Wordpress";
    }
}
const addImages = function (imagesArray) {
    imagesArray.forEach(imageObj => {
        let img = document.createElement('img');
        img.src = imageObj.url;
        img.alt = imageObj.name;
        img.classList.add('amazing-img');
        img.setAttribute('data-design', imageObj.dataAttributes['data-design']);
        container.appendChild(img);

        let creativeItem = document.createElement('div');
        creativeItem.innerHTML = ` <div class="circles">
                            <a href="#" class="circle first-circle">
                                <svg class="chain" width="15" height="15" viewBox="0 0 15 15" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                          d="M13.9131 2.72817L12.0948 0.891285C11.2902 0.0808612 9.98305 0.0759142 9.17681 0.882615L7.15921 2.89256C6.35161 3.69885 6.34818 5.01032 7.15051 5.82074L8.30352 4.68897C8.18678 4.32836 8.33041 3.9153 8.61593 3.62946L9.89949 2.35187C10.3061 1.94624 10.9584 1.94913 11.3595 2.35434L12.4513 3.45805C12.8528 3.86283 12.8511 4.51713 12.447 4.92318L11.1634 6.20241C10.8918 6.47296 10.4461 6.62168 10.1002 6.52626L8.97094 7.65887C9.77453 8.47177 11.0803 8.47466 11.8889 7.66837L13.9039 5.65924C14.7141 4.85254 14.7167 3.53983 13.9131 2.72817ZM6.52613 10.0918C6.62191 10.4441 6.46857 10.8997 6.19093 11.1777L4.99227 12.3752C4.58074 12.7845 3.91595 12.7833 3.50671 12.369L2.39297 11.2475C1.98465 10.8349 1.98729 10.1633 2.39824 9.75473L3.59804 8.55769C3.89032 8.26607 4.31044 8.12025 4.67711 8.24375L5.83354 7.0715C5.01493 6.2462 3.68249 6.24207 2.86059 7.06324L0.915197 9.0042C0.0922615 9.8266 0.0883685 11.1629 0.90651 11.9886L2.75817 13.8618C3.57595 14.6846 4.90724 14.6912 5.73111 13.8701L7.67649 11.9287C8.49852 11.1054 8.5024 9.77166 7.68553 8.9443L6.52613 10.0918ZM6.25787 9.56307C5.98013 9.84189 5.53427 9.84105 5.26179 9.55812C4.98792 9.27434 4.9901 8.82039 5.26613 8.53993L8.59075 5.16109C8.86679 4.88227 9.31174 4.88311 9.58513 5.16398C9.86048 5.44569 9.85876 5.90088 9.5817 6.18299L6.25787 9.56307Z"/>
                                </svg>
                            </a>
                            <a href="#" class="circle second-circle">
                                <svg class="square" width="12" height="11" viewBox="0 0 12 11" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <rect width="12" height="11"/>
                                </svg>
                            </a>
                        </div>
                        <h3 class="amazing-title">${resStr(String(imageObj.dataAttributes['data-design']))}</h3>
                        <p class="amazing-text">Web Design</p>
                    </div>`;
        creativeItem.classList.add('creative-item');
        creativeItem.style.display = 'none';

        container.appendChild(img);
        container.appendChild(creativeItem);

        img.addEventListener('mouseenter', function() {
            img.style.display = 'none';
            creativeItem.style.display = 'block';
        });

        creativeItem.addEventListener('mouseleave', function() {
            img.style.display = 'block';
            creativeItem.style.display = 'none';
        });
    });
}



const printCard = function (attribute) {
    container.innerHTML = "";
    if (attribute === "1") {
        loadButton.style.display = 'block'
    } else {
        loadButton.style.display = 'none'
    }

    const filterArr = loadMore.filter((card, index) => {
        if(attribute === "1" && index < 12) {
            return true;
        } else if (attribute === 'allCards') {
            return true;
        } else {
            return card.dataAttributes['data-design'] === attribute;
        }
    })

    addImages(filterArr);
}

function applyFilter() {
    amazingItems.forEach(item => {
        item.addEventListener('click', event => {
            const designNum = event.currentTarget.getAttribute('data-design');
            printCard(designNum);
        });
    });
}

loadButton.addEventListener('click', event => {
    const allImg = document.querySelector('.amazing-item');
    allImg.setAttribute('data-design' , 'allCards')
    printCard('allCards');
});


printCard('1');
applyFilter();


$(document).ready(function(){
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });

    let sliderNav = $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: true,
        focusOnSelect: true,
        nextArrow: '<div class="slick-item"><svg class="arr-img h="8" height="13" viewBox="0 0 8 13" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M1.49213 0.48752L0.18704 1.88987L4.47503 6.49714L0.18704 11.1056L1.49213 12.5084L7.08508 6.49714L1.49213 0.48752Z" /></svg></div>',
        prevArrow: '<div class="slick-item"><svg class="arr-img" height="13" viewBox="0 0 8 13" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M6.50764 12.5076L7.81261 11.1056L3.52463 6.49631L7.81261 1.88987L6.50764 0.486695L0.91457 6.49631L6.50764 12.5076Z" /></svg></div>',

    });

    function updateText(slideIndex) {
        $('.people-text, .people-subtext').hide(); // скрываем все тексты и подтексты
        const textKey = $(`.slider-for div[data-slick-index=${slideIndex}] img`).attr('data-text');
        $(`.people-text[data-text=${textKey}], .people-subtext[data-text=${textKey}]`).show(); // показываем текст и подтекст, соответствующий текущему слайду
    }

    updateText(0);

    sliderNav.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        updateText(nextSlide);
    });

});